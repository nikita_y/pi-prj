class Example_it():
    def __init__(self, max):
        self.max = max
        self.counter = 0

    def __next__(self):
        if self.counter < self.max:
            ret = self.counter
            self.counter += 1
            return ret
        else:
            raise StopIteration

    def __iter__(self):
        if self.max <= 0:
            raise AttributeError

        return self
