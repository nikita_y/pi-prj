#!/usr/local/bin/python3

# import pdb as p
import pprint


class Node():
    """Example of a binary tree"""

    def __init__(self, val, r=None, l=None):
        self.val = val
        self.r = r
        self.l = l

    def add_node(self, val):
        if val > self.val:
            if self.r == None:
                self.r = Node(val)
            else:
                self.r.add_node(val)
        elif val < self.val:
            if self.l == None:
                self.l = Node(val)
            else:
                # p.set_trace()
                self.l.add_node(val)
        else:
            raise ValueError('Value already exists')

    def __str__(self):
        return "{} {} {}".format(self.l if self.l else "",
                                 self.val,
                                 self.r if self.r else "")


def main():
    root = Node(5)
    root.add_node(4)
    root.add_node(6)
    root.add_node(1)

    print(root)


if __name__ == '__main__':
    main()


# for iplicitly calls iter() method
# type can be iterable __iter__ but not iterator __next__n
