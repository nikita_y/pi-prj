"""An example of how to setup and start an Accessory.
This is:
1. Create the Accessory object you want.
2. Add it to an AccessoryDriver, which will advertise it on the local network,
    setup a server to answer client queries, etc.
"""
import logging
import random
import signal
import time

import pyhap.loader as loader
from pyhap import camera
from pyhap.accessory import Accessory, Bridge
from pyhap.accessory_driver import AccessoryDriver
from pyhap.const import CATEGORY_OTHER, CATEGORY_SENSOR, CATEGORY_SWITCH

import RPi.GPIO as GPIO

logging.basicConfig(level=logging.INFO, format="[%(module)s] %(message)s")

GPIO.setmode(GPIO.BOARD)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)


def set_switch_on(is_on):
    if is_on == True:
        logging.info("Switch turn ON")
        GPIO.output(8, GPIO.LOW)
    else:
        logging.info("Switch turn OFF")
        GPIO.output(8, GPIO.HIGH)


def set_button_state(time_s):
    logging.info("Switch turn ON for {0}s".format(time_s))
    GPIO.output(10, GPIO.LOW)
    time.sleep(time_s)
    logging.info("Switch turn OFF")
    GPIO.output(10, GPIO.HIGH)


class CoffeeMaker(Accessory):
    category = CATEGORY_SWITCH

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        on_off = self.add_preload_service('Switch')
        self.on_off = on_off.configure_char("On")
        self.on_off_char = on_off.get_characteristic('On')
        self.on_off_char.setter_callback = self.on_off_clicked

        push_button = self.add_preload_service('Switch')
        self.push_button = push_button.configure_char("On")
        self.push_button_char = push_button.get_characteristic('On')
        self.push_button_char.setter_callback = self.push_button_event

    def on_off_clicked(self, value):
        set_switch_on(value)

    def push_button_event(self, value):
        set_button_state(0.05)
        time.sleep(1)
        set_button_state(0.05)
        time.sleep(1)
        set_button_state(0.05)
        time.sleep(1)
        self.push_button_char.set_value(False)

    def stop(self):
        logging.info("Clean up GPIO")
        GPIO.cleanup()


def get_accessory(driver):
    """Call this method to get a standalone Accessory."""
    return CoffeeMaker(driver, 'Breville BES870XL')


# Start the accessory on port 51826
driver = AccessoryDriver(port=51826)

# Change `get_accessory` to `get_bridge` if you want to run a Bridge.
driver.add_accessory(accessory=get_accessory(driver))

# We want SIGTERM (terminate) to be handled by the driver itself,
# so that it can gracefully stop the accessory, server and advertising.
signal.signal(signal.SIGTERM, driver.signal_handler)

# Start it!
driver.start()
