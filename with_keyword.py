# class FooType(object):
#     def __init__(self, id):
#         self.id = id
#         print(self.id, 'born')

#     def __del__(self):
#         print(self.id, 'died')

# #==================================
# # Recommended to use "with" always if applicable.
# #==================================


class controlled_execution():
    def __init__(self, id):
        print("__init__")
        self.id = id

    def __enter__(self):
        print("__enter__")
        self.id += 1
        return self

    def __exit__(self, type, value, traceback):
        print("__exit__")
        self.id += 100

    def __del__(self):
        print("__del__")
        self.id += 1000
        print(self.id)


def my_decorator(f):
    def wrapper(x):
        print("before")
        f(x)
        print("after")
    return wrapper


with controlled_execution(10) as thing:
    print(thing.id)

print(thing.id)
del thing


@my_decorator
def some_f(x):
    print("now: {}".format(x))


some_f(10)
