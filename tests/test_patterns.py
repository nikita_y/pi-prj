import unittest
import primer_iterators as it
import learning_patterns as lp


class TestDecorator(unittest.TestCase):
    def test_ask_a_duck_to_meow(self):
        is_error = False
        duck = lp.Duck()
        try:
            duck.meow()
        except AttributeError:
            is_error = True

        self.assertTrue(is_error)


class TestIterators(unittest.TestCase):
    def test_iter(self):
        data = [x for x in it.Example_it(10)]
        sample_set = [x for x in range(10)]
        self.assertListEqual(data, sample_set)

    def test_iter_except(self):
        is_error = False
        to_break = it.Example_it(0)

        try:
            data = [x for x in to_break]
        except AttributeError:
            is_error = True

        self.assertTrue(is_error)
