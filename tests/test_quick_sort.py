import random as rn
import unittest

import quick_sort as qs


class TestQuickSort(unittest.TestCase):
    def test_predefined(self):
        self.assertListEqual(qs.q_sort([44, 12, 11, 3]), [3, 11, 12, 44])

    def test_random(self):
        sample_set = rn.sample(range(1, 100), 10)
        self.assertListEqual(qs.q_sort(sample_set), sorted(sample_set))

    def test_negative(self):
        sample_set = rn.sample(range(-100, 100), 10)
        self.assertListEqual(qs.q_sort(sample_set), sorted(sample_set))

    def test_single(self):
        sample_set = [1]
        self.assertListEqual(qs.q_sort(sample_set), sorted(sample_set))

    def test_empty(self):
        sample_set = []
        self.assertListEqual(qs.q_sort(sample_set), sorted(sample_set))
