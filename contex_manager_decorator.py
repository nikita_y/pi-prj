from contextlib import contextmanager


class SomeObj():
    """docstring for SomeObj"""

    def __init__(self):
        self.arg = 10


@contextmanager
def some_func():
    print("before")
    yield SomeObj()
    print("after")


def main():
    with some_func() as f:
        print(f.arg)


if __name__ == '__main__':
    main()
