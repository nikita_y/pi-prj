import asyncio


async def func_one():
    print("Enetering func_one()")
    # do something time consuming:
    await asyncio.sleep(2)
    print("Leaving func_one()")


async def func_two():
    print("Enetering func_two()")
    # do something time consuming:
    await asyncio.sleep(1)
    print("Leaving func_two()")


async def func_three():
    await func_one()
    await func_two()


def main():
    # Getting instance of event loop
    loop = asyncio.get_event_loop()
    # transform functions to tasks
    tasks = [
        loop.create_task(func_one()),
        loop.create_task(func_two()),
        loop.create_task(func_three())
    ]
    # adding tasks in the eventloop
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()


if __name__ == "__main__":
    main()
