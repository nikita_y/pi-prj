#!/usr/local/bin/python3

def count_bits(x):
    count = 0
    while x:
        count += x & 1 # & checks if lowest bit is 1
        x >>= 1
    return count

def parity(x):
    res = 0
    while x:
        res ^= x & 1 # res ^= expr: res turns 1 every odd 1's apperance XOR(1,!) == 0 
        x >>= 1
    return res


if __name__ == '__main__':
    print(count_bits(3))
    print(parity(64))
