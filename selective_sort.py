import random as r


def smallest(arr):
    min_val = arr[0]
    min_idx = 0
    for i in range(1, len(arr)):
        if min_val < arr[i]:
            min_val = arr[i]
            min_idx = i

    return min_idx


def biggest(arr):
    max_val = arr[0]
    max_idx = 0
    for i in range(1, len(arr)):
        if max_val > arr[i]:
            max_val = arr[i]
            max_idx = i

    return max_idx


def s_sort(arr, f):
    sorted_arr = []
    for _ in range(0, len(arr)):
        idx = f(arr)
        sorted_arr.append(arr.pop(idx))

    return sorted_arr
